"use strict";

const API_KEY = "3e441eaa28e2813aa9e69d67561a8c08" ;

class WeatherNode extends React.Component {
    constructor(props) {
      super(props);
      this.city=props.city;
      this.state = {
          imgSrc: '',
          weatherText: "Please wait...",
          temp: ""
      }
    }
  
   componentDidMount() {
    fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&appid=${API_KEY}`)
    .then( response => {
        if (!response.ok) {
            throw new Error('Error code:'  + response.status);
        }
        return response.json();
    })
    .then ( json => this.update(json)  )
    .catch( error => this.update("error") ) ;
   }

   update(obj){
       if(obj == "error"){
        this.setState ( {
            imgSrc: "",
            weatherText: "Weather not available",
            temp: ""
        });
       }else{
        this.setState ( {
            imgSrc: `http://openweathermap.org/img/wn/${obj.weather[0].icon}@2x.png`,
            weatherText: obj.weather[0].description,
            temp: `${obj.main.temp} C`
        });
       }
   }
    render() { 
         return (
            <div style= {{border: 'solid 5px black', width: '20%',textAlign: 'center',borderRadius: "10px"}}   >
             <h2>{this.state.weatherText}  {this.state.temp}</h2>
             <img src={this.state.imgSrc} ></img>
             <button onClick={this.componentDidMount}>Refresh</button>
            </div>
        );
        
    }
  }
  


  ReactDOM.render(<WeatherNode  city='Montreal'/>,  document.getElementById('container'));